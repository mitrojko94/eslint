import { SectionCreator } from "./join-us-section.js";
import "./styles/style.css";

window.addEventListener("load", () => {
  const creator = new SectionCreator();
  const newSection = creator.create("standard");
  console.log(newSection); // eslint-disable no-console
  // NewSection.remove();
});
